package com.oleh.controller;

import com.oleh.model.Model;

import java.io.IOException;

public class Controller implements ControllerInt {
    Model model;

    public Controller() {
        this.model = new Model();
    }

    @Override
    public void testSimplePingPong() {
        model.simplePingPong();
    }

    @Override
    public void testFibonacci() {
        model.fibonacci();
    }

    @Override
    public void testFibonacciWithExecutors() {
        model.fibonacciWithExecutors();
    }

    @Override
    public void testFibonacciWithCallable() throws InterruptedException {
        model.fibonacciWithCallable();
    }

    @Override
    public void testSleeper(int amountOfSleepers) {
        model.sleeper(amountOfSleepers);
    }

    @Override
    public void testThreeSyncMethodsTest() {
        model.threeSyncMethodsTest();
    }

    @Override
    public void testThreeAsyncMethodsTest() {
        model.threeAsyncMethodsTest();
    }

    @Override
    public void testPipeCommunication() throws IOException {
        model.pipeCommunication();
    }

    @Override
    public void testLocks() {
        model.taskWithLocks();
    }

    @Override
    public void testBlockingQueueCommunication() {
        model.blockingQueueCommunication();
    }

    @Override
    public void testMyReadWriteLock() {
        model.testMyReadWriteLock();
    }
}
