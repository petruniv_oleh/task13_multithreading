package com.oleh.controller;

import java.io.IOException;

public interface ControllerInt {

    void testSimplePingPong();
    void testFibonacci();
    void testFibonacciWithExecutors();
    void testFibonacciWithCallable() throws InterruptedException;
    void testSleeper(int amountOfSleepers);
    void testThreeSyncMethodsTest();
    void testThreeAsyncMethodsTest();
    void testPipeCommunication() throws IOException;
    void testLocks();
    void testBlockingQueueCommunication();
    void testMyReadWriteLock();
}
