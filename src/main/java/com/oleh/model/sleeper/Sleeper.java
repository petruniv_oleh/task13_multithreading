package com.oleh.model.sleeper;

import java.util.Random;

public class Sleeper implements Runnable{

    @Override
    public void run() {
        Random r = new Random();
        long start = System.nanoTime();
        long sleepingTime = r.nextInt(10000) + 1000;
        try {
            Thread.sleep(sleepingTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+" sleeps for: "+sleepingTime/1000
                +"s and exist for: "+((System.nanoTime()-start)/1000000000)+"s");
    }
}
