package com.oleh.model.syncMethods;

public class AsyncMethodsClass implements Runnable {

    private static Object firstMonitor = new Object();
    private static Object secondMonitor = new Object();
    private static Object thirdMonitor = new Object();


    public void firstMethod(){
        synchronized (firstMonitor){
            System.out.println(Thread.currentThread().getName()+" - is in first method");
        }
    }

    public void secondMethod(){
        synchronized (secondMonitor){
            System.out.println(Thread.currentThread().getName()+" - is in second method");
        }
    }

    public void thirdMethod(){
        synchronized (thirdMonitor){
            System.out.println(Thread.currentThread().getName()+" - is in third method");
        }
    }

    @Override
    public void run() {
        firstMethod();
        secondMethod();
        thirdMethod();
    }
}
