package com.oleh.model.syncMethods;

public class SyncMethodsClass implements Runnable{
    private static Object monitor = new Object();


    public void firstMethod(){
        synchronized (monitor){
            System.out.println(Thread.currentThread().getName()+" - is in first method");
        }
    }

    public void secondMethod(){
        synchronized (monitor){
            System.out.println(Thread.currentThread().getName()+" - is in second method");
        }
    }

    public void thirdMethod(){
        synchronized (monitor){
            System.out.println(Thread.currentThread().getName()+" - is in third method");
        }
    }


    @Override
    public void run() {
        firstMethod();
        secondMethod();
        thirdMethod();
    }
}
