package com.oleh.model;

import com.oleh.model.communication.MessageReciver;
import com.oleh.model.communication.MessageSender;
import com.oleh.model.fibonacci.FibonacciCallable;
import com.oleh.model.fibonacci.FibonacciRunnable;
import com.oleh.model.readWriteLock.MyReadWriteLock;
import com.oleh.model.sleeper.Sleeper;
import com.oleh.model.syncMethods.AsyncMethodsClass;
import com.oleh.model.syncMethods.SyncMethodsClass;
import com.oleh.model.syncWithLocks.SyncLockMethodsClass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Model implements ModelInt {
    private static Object pingPongMonitor = new Object();

    /**
     * Simple "ping-pong" example
     */
    public void simplePingPong() {
        Thread ping = new Thread(() -> {
            synchronized (pingPongMonitor) {

                for (int i = 0; i < 10; i++) {
                    try {
                        pingPongMonitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + ": ping");
                    pingPongMonitor.notify();
                }

                System.out.println(Thread.currentThread().getName() + " end");
            }
        });

        Thread pong = new Thread(() -> {
            synchronized (pingPongMonitor) {
                for (int i = 0; i < 10; i++) {
                    pingPongMonitor.notify();
                    System.out.println(Thread.currentThread().getName() + ": pong");
                    try {
                        pingPongMonitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName() + " end");
            }
        });

        ping.start();
        pong.start();
        try {
            ping.join();
            pong.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Here are producing of fibonacci numbers with 3 threads
     */
    public void fibonacci() {
        Thread fibonacci1 = new Thread(new FibonacciRunnable(10));
        Thread fibonacci2 = new Thread(new FibonacciRunnable(10));
        Thread fibonacci3 = new Thread(new FibonacciRunnable(10));

        fibonacci1.start();
        fibonacci2.start();
        fibonacci3.start();

        try {
            fibonacci1.join();
            fibonacci2.join();
            fibonacci3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Here are producing of fibonacci numbers with executor
     */
    public void fibonacciWithExecutors() {
        Executor executor1 = Executors.newSingleThreadExecutor();
        executor1.execute(new FibonacciRunnable(7));
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new FibonacciRunnable(7));
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.schedule(new FibonacciRunnable(7), 1000, TimeUnit.MICROSECONDS);


        executorService.shutdown();
    }

    /**
     * Here are producing of fibonacci number and summing them
     */
    @Override
    public void fibonacciWithCallable() throws InterruptedException {
        List<FibonacciCallable> callables = new ArrayList<>();
        callables.add(new FibonacciCallable(7));
        callables.add(new FibonacciCallable(7));
        callables.add(new FibonacciCallable(7));

        ExecutorService executorService = Executors.newScheduledThreadPool(3);

        executorService.invokeAll(callables)
                .stream()
                .map(f -> {
                    try {
                        return f.get();
                    } catch (Exception e) {
                        throw new IllegalStateException(e);
                    }
                }).forEach(System.out::println);
    }

    /**
     * Testing sleeping threads
     *
     * @param amountOfSleepers
     */
    @Override
    public void sleeper(int amountOfSleepers) {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(amountOfSleepers);
        for (int i = 0; i < amountOfSleepers; i++) {
            scheduledExecutorService.schedule(new Sleeper(), 1000, TimeUnit.MICROSECONDS);
        }

    }

    /**
     * Test threads that worked with synchronized methods
     */
    @Override
    public void threeSyncMethodsTest() {
        Thread thread1 = new Thread(new SyncMethodsClass());
        Thread thread2 = new Thread(new SyncMethodsClass());
        Thread thread3 = new Thread(new SyncMethodsClass());

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    /**
     * Test threads that worked with synchronized methods with different monitors
     */
    @Override
    public void threeAsyncMethodsTest() {
        Thread thread1 = new Thread(new AsyncMethodsClass());
        Thread thread2 = new Thread(new AsyncMethodsClass());
        Thread thread3 = new Thread(new AsyncMethodsClass());

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test threads communication with pipe
     *
     * @throws IOException
     */
    @Override
    public void pipeCommunication() throws IOException {
        MessageSender messageSender = new MessageSender();
        MessageReciver messageReciver = new MessageReciver(messageSender.getPipedWriter());
        Thread thread1 = new Thread(messageSender);
        Thread thread2 = new Thread(messageReciver);

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    /**
     * Sync using lock
     */
    @Override
    public void taskWithLocks() {
        Thread thread1 = new Thread(new SyncLockMethodsClass());
        Thread thread2 = new Thread(new SyncLockMethodsClass());
        Thread thread3 = new Thread(new SyncLockMethodsClass());

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void blockingQueueCommunication() {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(10);

        Thread t1 = new Thread(() -> {
            blockingQueue.add("Hello? I'am " + Thread.currentThread().getName());
            blockingQueue.add("How are you");
            try {
                while (!blockingQueue.isEmpty()) {
                    System.out.println(blockingQueue.take());
                }
                Thread.sleep(500);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {

            try {
                Thread.sleep(500);
                blockingQueue.add("Hi! I'am " + Thread.currentThread().getName());
                while (!blockingQueue.isEmpty()) {
                    System.out.println(blockingQueue.take());
                }

                blockingQueue.add("Hi again");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void testMyReadWriteLock() {
        MyReadWriteLock myReadWriteLock = new MyReadWriteLock();
        AtomicInteger a = new AtomicInteger();

        Thread reader = new Thread(() -> {
            for (int i = 0; i < 3; i++) {
                myReadWriteLock.readLock();
                System.out.println("A = "+a);
                myReadWriteLock.readUnlock();
            }
        });
        Thread writer = new Thread(()->{
            for (int i = 0; i < 9; i++) {
                myReadWriteLock.writeLock();
                a.getAndIncrement();
                if (i%2==0){
                    try {
                        myReadWriteLock.wtiteUnlock();
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        reader.start();
        writer.start();
        try {
            reader.join();
            writer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
