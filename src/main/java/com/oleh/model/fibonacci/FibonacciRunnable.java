package com.oleh.model.fibonacci;

import java.util.ArrayList;
import java.util.List;

public class FibonacciRunnable implements Runnable {
    private int n;
    private List<Integer> fibonacciNumbers;
    public FibonacciRunnable(int n) {
        this.n = n;
        fibonacciNumbers = new ArrayList<>();

    }

    private void makeFibonacciList(){
        fibonacciNumbers.add(1);
        System.out.println(Thread.currentThread().getName()+": "+fibonacciNumbers.get(0));
        fibonacciNumbers.add(1);
        System.out.println(Thread.currentThread().getName()+": "+fibonacciNumbers.get(1));
        for (int i = 2; i < n; i++) {
            fibonacciNumbers.add(fibonacciNumbers.get(i-1)+fibonacciNumbers.get(i-2));
            System.out.println(Thread.currentThread().getName()+": "+fibonacciNumbers.get(i));
        }

    }


    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+" start ");
        makeFibonacciList();
        System.out.println(Thread.currentThread().getName()+" end ");
    }
}
