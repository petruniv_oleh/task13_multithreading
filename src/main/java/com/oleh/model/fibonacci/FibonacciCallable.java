package com.oleh.model.fibonacci;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class FibonacciCallable implements Callable<Integer> {
    private int n;
    private List<Integer> fibonacciNumbers;
    private int sum;

    public FibonacciCallable(int n) {
        sum = 0;
        this.n = n;
        fibonacciNumbers = new ArrayList<>();

    }

    private void makeFibonacciList() {
        fibonacciNumbers.add(1);
        sum += fibonacciNumbers.get(0);
        System.out.println(Thread.currentThread().getName() + ": " + fibonacciNumbers.get(0));
        fibonacciNumbers.add(1);
        sum += fibonacciNumbers.get(1);
        System.out.println(Thread.currentThread().getName() + ": " + fibonacciNumbers.get(1));

        for (int i = 2; i < n; i++) {
            fibonacciNumbers.add(fibonacciNumbers.get(i - 1) + fibonacciNumbers.get(i - 2));
            sum += fibonacciNumbers.get(i);
            System.out.println(Thread.currentThread().getName() + ": " + fibonacciNumbers.get(i));
        }
    }


    @Override
    public Integer call() throws Exception {
        makeFibonacciList();
        return sum;
    }
}
