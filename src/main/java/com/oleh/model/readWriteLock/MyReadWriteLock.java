package com.oleh.model.readWriteLock;

public class MyReadWriteLock {
    private int readRequest;
    private int writeRequest;

    public synchronized void readLock(){
        while (writeRequest>0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        readRequest++;
    }

    public synchronized void writeLock(){
        while (readRequest>0||writeRequest>0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        writeRequest++;
    }

    public synchronized void readUnlock(){
        if (readRequest!=0) {
            readRequest--;
        }
        notifyAll();
    }

    public synchronized void wtiteUnlock(){
        if (writeRequest!=0){
            writeRequest--;
        }
        notifyAll();
    }

}
