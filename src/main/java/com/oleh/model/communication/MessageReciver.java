package com.oleh.model.communication;

import java.io.*;

public class MessageReciver implements Runnable{
    private PipedInputStream pipedReader;

    public MessageReciver(PipedOutputStream pipedWriter) throws IOException {
        this.pipedReader = new PipedInputStream(pipedWriter);
    }


    @Override
    public void run() {
        try {
            byte[] msg = new byte[1024];
            pipedReader.read(msg);
            String s = new String(msg);
            System.out.println(Thread.currentThread().getName()+" received: "+ s);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
