package com.oleh.model.communication;

import java.io.IOException;
import java.io.PipedOutputStream;

public class MessageSender implements Runnable{
    private PipedOutputStream pipedWriter = new PipedOutputStream();

    public PipedOutputStream getPipedWriter() {
        return pipedWriter;
    }

    @Override
    public void run() {
        try {
            pipedWriter.write(("Hello, i`m "+ Thread.currentThread().getName()+", and i'm sending some information!").getBytes());
            System.out.println(Thread.currentThread().getName()+" sent message!");
            Thread.sleep(1000);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
