package com.oleh.model.syncWithLocks;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SyncLockMethodsClass implements Runnable{
    Lock lock = new ReentrantLock();


    public void firstMethod(){
            System.out.println(Thread.currentThread().getName()+" - is in first method");
    }

    public void secondMethod(){
            System.out.println(Thread.currentThread().getName()+" - is in second method");

    }

    public void thirdMethod(){

            System.out.println(Thread.currentThread().getName()+" - is in third method");

    }


    @Override
    public void run() {
        try{
            lock.lock();
            firstMethod();
            secondMethod();
            thirdMethod();
        }finally {
            lock.unlock();
        }

    }
}
