package com.oleh.model;

import java.io.IOException;

public interface ModelInt {
    void simplePingPong();
    void fibonacci();
    void fibonacciWithExecutors();
    void fibonacciWithCallable() throws InterruptedException;
    void sleeper(int amountOfSleepers);
    void threeSyncMethodsTest();
    void threeAsyncMethodsTest();
    void pipeCommunication() throws IOException;
    void taskWithLocks();
    void blockingQueueCommunication();
    void testMyReadWriteLock();
}
