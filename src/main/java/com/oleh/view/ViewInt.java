package com.oleh.view;

public interface ViewInt {
    void showMenu();
    void testSimplePingPong();
    void testFibonacci();
    void testFibonacciWithExecutors();
    void testFibonacciWithCallable();
    void testSleeper();
    void testThreeSyncMethodsTest();
    void testThreeAsyncMethodsTest();
    void testPipeCommunication();
    void testLocks();
    void testBlockingQueueCommunication();
    void testReadWriteLock();
}
