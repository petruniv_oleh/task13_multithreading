package com.oleh.view;

import com.oleh.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View implements ViewInt {

    Controller controller;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private static Logger logger = LogManager.getLogger(View.class.getName());
    Scanner scanner = new Scanner(System.in);

    public View() {
        controller = new Controller();
        menuMap();
        showMenu();
    }

    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put("1", "1. Ping-pong");
        menuMap.put("2", "2. Normal fibonacci");
        menuMap.put("3", "3. Fibonacci With Executors");
        menuMap.put("4", "4. Fibonacci With Callable");
        menuMap.put("5", "5. Sleeper");
        menuMap.put("6", "6. Three Sync Methods Test");
        menuMap.put("7", "7. Three Async Methods Test");
        menuMap.put("8", "8. Pipe Communication");
        menuMap.put("9", "9. Test locks");
        menuMap.put("10", "10. Test communication with blocking queue");
        menuMap.put("11", "11. Test my read write lock");

        menuMapMethods.put("1", this::testSimplePingPong);
        menuMapMethods.put("2", this::testFibonacci);
        menuMapMethods.put("3", this::testFibonacciWithExecutors);
        menuMapMethods.put("4", this::testFibonacciWithCallable);
        menuMapMethods.put("5", this::testSleeper);
        menuMapMethods.put("6", this::testThreeSyncMethodsTest);
        menuMapMethods.put("7", this::testThreeAsyncMethodsTest);
        menuMapMethods.put("8", this::testPipeCommunication);
        menuMapMethods.put("9", this::testLocks);
        menuMapMethods.put("10", this::testBlockingQueueCommunication);

    }

    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    @Override
    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("Q"));
    }

    @Override
    public void testSimplePingPong() {
        logger.info("Invoke testSimplePingPong");
        controller.testSimplePingPong();
    }

    @Override
    public void testFibonacci() {
        logger.info("Invoke testFibonacci");
        controller.testFibonacci();
    }

    @Override
    public void testFibonacciWithExecutors() {
        logger.info("Invoke testFibonacciWithExecutors");
        controller.testFibonacciWithExecutors();
    }

    @Override
    public void testFibonacciWithCallable() {
        logger.info("Invoke testFibonacciWithCallable");
        try {
            controller.testFibonacciWithCallable();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void testSleeper() {
        logger.info("Invoke testSleeper");
        System.out.println("Enter the amount of threads: ");
        int amountOfSleepers = scanner.nextInt();
        controller.testSleeper(amountOfSleepers);

    }

    @Override
    public void testThreeSyncMethodsTest() {
        logger.info("Invoke testThreeSyncMethodsTest");
        controller.testThreeSyncMethodsTest();
    }

    @Override
    public void testThreeAsyncMethodsTest() {
        logger.info("Invoke testThreeAsyncMethodsTest");
        controller.testThreeAsyncMethodsTest();
    }

    @Override
    public void testPipeCommunication() {
        logger.info("Invoke testPipeCommunication");
        try {
            controller.testPipeCommunication();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void testLocks() {
        logger.info("Invoke testLocks");
        controller.testLocks();
    }

    @Override
    public void testBlockingQueueCommunication() {
        logger.info("Invoke testBlockingQueueCommunication");
        controller.testBlockingQueueCommunication();
    }

    @Override
    public void testReadWriteLock() {
        logger.info("Invoke testReadWriteLock");
        controller.testMyReadWriteLock();
    }
}
